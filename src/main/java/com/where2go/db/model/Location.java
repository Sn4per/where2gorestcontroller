package com.where2go.db.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.*;

import java.io.Serializable;
import java.util.Set;

@Node
@Getter
@Setter
@NoArgsConstructor
public class Location implements Serializable {
    @Id
    @GeneratedValue
    private Long locationID;

    @Property
    private String locationName;

    @Property
    private String locationCity;

    @Property
    private String locationStreet;

    @Property
    private int locationHouseNumber;

    @Property
    private String locationAppendix;

    @Relationship(type = "hostsEvents",direction = Relationship.Direction.OUTGOING)
    private Set<Event> events;


}
