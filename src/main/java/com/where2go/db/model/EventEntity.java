package com.where2go.db.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Tobias Frommhund (20140057)
 **/

@Accessors(chain = true)
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "eventId")
@ToString

//@Entity
@Table(name = "Event", schema = "public", catalog = "Where2Go")
public class EventEntity implements Serializable {
    @Id
    @Column(name = "EventID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long eventId;

    @Basic
    @Column(name = "EventName", nullable = false, length = 100)
    private String eventName;

    @Basic
    @Column(name = "beginDate", nullable = false)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime beginDate;

    @Basic
    @Column(name = "endDate", nullable = false)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endDate;

    @Basic
    @Column(name = "locationCity", nullable = false, length = 50)
    private String locationCity;

    @Basic
    @Column(name = "locationStreet", nullable = false, length = 50)
    private String locationStreet;

    @Basic
    @Column(name = "locationHouseNumber", nullable = false)
    private int locationHouseNumber;

    @Basic
    @Column(name = "isFree", nullable = true)
    private Boolean isFree;

    @Basic
    @Column(name = "isClub", nullable = true)
    private Boolean isClub;

    @Basic
    @Column(name = "isOutDoor", nullable = true)
    private Boolean isOutDoor;

    @Basic
    @Column(name = "isIndoor", nullable = true)
    private Boolean isIndoor;

    @Basic
    @Column(name = "lat", nullable = true, precision = 0)
    private Float latitude;

    @Basic
    @Column(name = "long", nullable = true, precision = 0)
    private Float longitude;
}