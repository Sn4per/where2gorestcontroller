package com.where2go.db.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Node("Event")
public class Event implements Serializable{

    @Id
    @GeneratedValue
    private Long eventID;


    @Property
    private String eventName;


    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Property
    private LocalDateTime beginDate;


    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Property
    private LocalDateTime endDate;

    @Relationship(type = "isOnDate",direction = Relationship.Direction.OUTGOING)
    private Date relDate;

    @Relationship(type = "hostsEvents",direction = Relationship.Direction.INCOMING)
    private Location location;

    @Property
    private Boolean isFree;


    @Property
    private Boolean isClub;


    @Property
    private Boolean isOutDoor;


    @Property
    private Boolean isIndoor;

    public Event(String eventName, Date beginDate){
        this.relDate = beginDate;
        this.eventName = eventName;
    }
}
