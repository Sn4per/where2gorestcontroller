package com.where2go.db.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
@Builder

public class Message implements Serializable{
    String command;
    String returnQueue;
    UUID requestID;


    Map<String,Serializable> arguments;

    public Message(String command, UUID requestID) {
        this.command = command;
        this.requestID = requestID;
    }

    public Message(String command, String returnQueue, UUID requestID, Map<String, Serializable> arguments) {
        this.command = command;
        this.returnQueue = returnQueue;
        this.arguments = arguments;
        this.requestID = requestID;
    }
    public Message(){
        arguments = new HashMap<>();
    }

}
