package com.where2go.db.redis;

import com.where2go.db.controller.EventRepository;
import com.where2go.db.controller.MessageRepository;
import com.where2go.db.model.Message;
import com.where2go.db.redis.generic.MessageProcessingService;
import com.where2go.db.redis.generic.OutBoundGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service("RedisMessageProcessingService")
public class RedisMessageProcessingService implements MessageProcessingService {

    Map<UUID,CompletableFuture<Message>> messages = new HashMap<>();

    @Autowired
    private ApplicationContext context;

    @Autowired
    private RedisTemplate<String,Message> redisTemplate;

    @Autowired
    private MessageRepository repository;


    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private OutBoundGateway outBoundGateway;

    @Autowired
    private RedisMessageProcessingService processor;

    //@Autowired
    //private EventRepository eventRepository;


    @Override
    public void process(Message message) {
        UUID requestID = message.getRequestID();
        CompletableFuture<Message> returnMessage = messages.get(requestID);

        if(returnMessage != null){
            returnMessage.complete(message);
            messages.remove(requestID);
        }
        System.out.println(messages.size());

        //eventRepository.findEventEntitiesByBeginDateBetween();


    }
    public void addCompletableFuture(UUID uuid,CompletableFuture<Message> message){
        messages.put(uuid,message);
    }
}
