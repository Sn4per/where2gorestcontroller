package com.where2go.db.redis;

import com.where2go.db.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service("RedisInBoundGateway")
public class RedisInBoundGateway {

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private RedisTemplate<String,Message> redisTemplate;

    @Autowired
    private RedisMessageProcessingService processingService;


    public void setupQueueListener(){

        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                while(!Thread.currentThread().isInterrupted()){
                    Message m = redisTemplate.opsForList().leftPop("RestGW1", Duration.ofMillis(10000));
                    if( m!=null){
                        processingService.process(m);
                    }

                }
            }
        });
    }
}
