package com.where2go.db.redis.generic;

import com.where2go.db.model.Message;

public interface OutBoundGateway {

    void push(String queue, Message event);
}
