package com.where2go.db.redis.generic;

import com.where2go.db.model.Message;

public interface MessageProcessingService {
    void process(Message event);
}
