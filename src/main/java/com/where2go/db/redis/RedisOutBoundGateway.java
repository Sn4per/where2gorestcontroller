package com.where2go.db.redis;

import com.where2go.db.model.Message;
import com.where2go.db.redis.generic.OutBoundGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;


public class RedisOutBoundGateway implements OutBoundGateway {

    @Autowired
    RedisTemplate<String,Message> redisTemplate;
    @Autowired
    private ApplicationContext context;


    //TODO make multithreaded

    @Override
    public void push(String queue,Message message) {
        redisTemplate = (RedisTemplate) context.getBean("redisTemplate");
        redisTemplate.opsForList().rightPush(queue,message);

    }








}
