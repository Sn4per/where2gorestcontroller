package com.where2go.db.redis;

import com.where2go.db.model.Message;
import com.where2go.db.redis.generic.OutBoundGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;


@Configuration
@EnableAutoConfiguration
public class RedisConfig {



    @Bean
    public RedisTemplate< String, Message > redisTemplate(RedisConnectionFactory connectionFactory) {
        final RedisTemplate< String,Message> template =  new RedisTemplate< String, Message >();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer( new StringRedisSerializer() );
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer( new GenericJackson2JsonRedisSerializer() );
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer() );
        return template;
    }

    @Bean
    public OutBoundGateway outBoundGateway(){
        return new RedisOutBoundGateway();
    }



    @Autowired
    public OutBoundGateway channelGateway;


}
