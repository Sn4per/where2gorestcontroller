package com.where2go.db.where2gorestcontroller;

import com.where2go.db.controller.EventRepository;
import com.where2go.db.controller.NodeEventRepository;
import com.where2go.db.model.EventEntity;
import com.where2go.db.model.Message;
import com.where2go.db.redis.RedisConfig;
import com.where2go.db.redis.RedisInBoundGateway;
import com.where2go.db.redis.RedisMessageProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.metrics.export.prometheus.EnablePrometheusMetrics;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@SpringBootApplication
@EnableNeo4jRepositories({ "com.where2go.db.controller"})
@ComponentScan(basePackages = { "com.where2go.db.redis","com.where2go.db.controller","com.where2go.db.rest" })
@EntityScan(value = {"com.where2go.db.data"},basePackageClasses = {EventEntity.class})
//@EnablePrometheusMetrics
public class Where2GoRestControllerApplication {

    @Value("${redis.queue}")
    public static String queue;

    @Autowired
    public static ApplicationContext context;

    @Autowired
    public static RedisMessageProcessingService queueListener;




    public static Map<String,String> startupConfig;

    public static void main(String[] args) {
        context = SpringApplication.run(Where2GoRestControllerApplication.class, args);

        RedisTemplate redisTemplate = (RedisTemplate) context.getBean("redisTemplate");
        RedisConfig redisConfig = (RedisConfig) context.getBean("redisConfig");

        //NodeEventRepository eventRepository = context.getBean(NodeEventRepository.class);
        RedisInBoundGateway listener = context.getBean(RedisInBoundGateway.class);
        listener.setupQueueListener();

        //redisConfig.channelGateway.push("Query", new Message("searchEventsByDate","RestGW1", UUID.randomUUID(),new HashMap<>()));
        /*context = SpringApplication.run(Where2GoRestControllerApplication.class, args);

        EventRepository eventRepository = context.getBean(EventRepository.class);

        eventRepository.save(new EventEntity(1l,"Vor der Schule saufen", LocalDateTime.of(2021,07,13,20,00),LocalDateTime.of(2021,07,14,04,00),"St. Pölten","Waldstrasse",3,true,false,true,false,0.0f,0.0f));

        RedisTemplate redisTemplate = (RedisTemplate) context.getBean("redisTemplate");
        RedisConfig redisConfig = (RedisConfig) context.getBean("redisConfig");
        System.out.println(redisTemplate.getConnectionFactory().getConnection().toString());
        HashMap<String, Serializable> arguments = new HashMap<>();
        LocalDateTime beginDate = LocalDateTime.of(2021,07,12,00,00);
        LocalDateTime endDate = LocalDateTime.of(2021,07,19,23,59);
        //arguments.put("timeQuery", new TimeQuery(beginDate,endDate));
        arguments.put("beginDate",beginDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        arguments.put("endDate",endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        redisConfig.channelGateway.push("Query", new Message("searchEventsByDate","RestGW1", UUID.randomUUID(),arguments));
        RedisInBoundGateway listener = context.getBean(RedisInBoundGateway.class);*/



    }





}
