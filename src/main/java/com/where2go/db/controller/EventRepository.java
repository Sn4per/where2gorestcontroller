package com.where2go.db.controller;

import com.where2go.db.model.EventEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<EventEntity, Long> {
    List<EventEntity> findEventEntitiesByBeginDateBetween(LocalDateTime start, LocalDateTime end);
   // List<EventEntity> findEventEntitiesByBeginDateAAndEventName(LocalDateTime start,"Kirtag");





}
