package com.where2go.db.controller;

import com.where2go.db.model.Date;
import com.where2go.db.model.Event;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
@EnableNeo4jRepositories(basePackageClasses=Neo4jRepository.class)
public interface NodeEventRepository extends Neo4jRepository<Event, Long> {

    Set<Event> findNodeEventsByRelDate_Date(Date beginDate);
    Set<Event> findNodeEventsByRelDateIn(List<Date> dates);

}
