package com.where2go.db.controller;


import com.where2go.db.model.Date;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Set;

@Repository
@EnableNeo4jRepositories(basePackageClasses= Neo4jRepository.class)
public interface NodeDateRepository extends Neo4jRepository<Date, LocalDate> {

    public Set<Date> findDateByDateBetween(LocalDate beginDate, LocalDate endDate);
    public Date findDateByDate(LocalDate date);
}
