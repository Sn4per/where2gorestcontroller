package com.where2go.db.config;

import com.where2go.db.model.EventEntity;
import com.where2go.db.redis.RedisConfig;
import com.where2go.db.rest.RestConfig;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@Import({EventEntity.class})
@ImportResource({"classpath:WEB-INF/redisConfig.xml", "classpath:WEB-INF/consumer.xml"})
@AutoConfigureAfter({RedisConfig.class, RestConfig.class})
public class AppConfig {





}
