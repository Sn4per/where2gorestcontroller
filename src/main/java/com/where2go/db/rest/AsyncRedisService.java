package com.where2go.db.rest;

import com.where2go.db.model.EventEntity;
import com.where2go.db.model.Message;
import com.where2go.db.redis.RedisMessageProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.ext.ParamConverter;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class AsyncRedisService {
    @Autowired
    @Lazy
    private RestTemplate restTemplate;

    @Autowired
    private RedisMessageProcessingService messageProcessingService;

  /*  @Autowired
    private TaskExecutor taskExecutor;*/

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
    @Async
    public CompletableFuture<Message> waitForMessage(UUID requestUUID) throws InterruptedException{
        CompletableFuture<Message> message = new CompletableFuture<>();
        messageProcessingService.addCompletableFuture(requestUUID,message);
        return message;
    }
}
