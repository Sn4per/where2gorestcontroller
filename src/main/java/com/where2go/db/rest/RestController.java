package com.where2go.db.rest;


import com.where2go.db.model.Event;
import com.where2go.db.model.Message;
import com.where2go.db.redis.generic.OutBoundGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired
    private ApplicationContext context;
    @Autowired
    public OutBoundGateway outBoundGateway;

    @Autowired
    public AsyncRedisService service;





    @GetMapping("/search")
    List<Event> getEntities(@RequestParam(value="beginDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate beginDate, @RequestParam(value="endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        System.out.println(beginDate + " " + endDate);

        System.out.println(context);
        System.out.println(outBoundGateway);
        System.out.println(service);
        Map<String, Serializable> arguments = new HashMap<>();
        arguments.put("beginDate",beginDate.atStartOfDay().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        arguments.put("endDate",endDate.atTime(23,59).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));

        UUID requestID = UUID.randomUUID();
        List<Event> entities = new ArrayList<>();
        outBoundGateway.push("Query", new Message("searchEventsByDate","RestGW1", requestID ,arguments));
        try {
            CompletableFuture<Message> returnMessage = service.waitForMessage(requestID);


            for(Serializable s :returnMessage.get().getArguments().values()){
                entities.add((Event) s);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    return entities;
    }
}
